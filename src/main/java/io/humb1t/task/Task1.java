package io.humb1t.task;

import io.humb1t.file.FileSource;
import io.humb1t.file.factory.FileFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Task1 {
    private static final String FILE_NAME = "verse.txt";
    private static final FileSource FILE_SOURCE = FileSource.RESOURCE;

    public static void main(String[] args) {
        Task1 main = new Task1();
        FileFactory fileFactory = new FileFactory();
        File file = fileFactory.getFile(FILE_SOURCE, FILE_NAME);
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            StringBuilder builder = new StringBuilder();
            String nextLine;
            while ((nextLine = reader.readLine()) != null) {
                if (builder.length() != 0) {
                    builder.append("\n");
                }
                builder.append(nextLine);
            }
            main.printReversed(builder.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void printReversed(String line) {
        System.out.println(new StringBuilder(line).reverse());
    }
}
