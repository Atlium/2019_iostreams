package io.humb1t.task;

import io.humb1t.file.FileSource;
import io.humb1t.file.factory.FileFactory;

import java.io.*;

public class Task2 {
    private static final String INPUT_FILE_NAME = "verse.txt";
    private static final String OUTPUT_FILE_NAME = "reversedVerse.txt";
    private static final FileSource INPUT_FILE_SOURCE = FileSource.RESOURCE;
    private static final FileSource OUTPUT_FILE_SOURCE = FileSource.FILE_SYSTEM;

    public static void main(String[] args) {
        Task2 main = new Task2();
        FileFactory fileFactory = new FileFactory();
        File inputFIle = fileFactory.getFile(INPUT_FILE_SOURCE, INPUT_FILE_NAME);
        File outputFile = fileFactory.getFile(OUTPUT_FILE_SOURCE, OUTPUT_FILE_NAME);
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFIle));
             BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {
            String nextLine;
            while ((nextLine = reader.readLine()) != null) {
                writer.write(new StringBuilder(nextLine).reverse().toString());
                writer.newLine();
            }
            writer.flush();
            main.printFile(outputFile);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void printFile(File file) {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String nextLine;
            while ((nextLine = reader.readLine()) != null) {
                System.out.println(nextLine);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
