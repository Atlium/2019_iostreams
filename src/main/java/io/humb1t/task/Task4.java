package io.humb1t.task;

import io.humb1t.file.FileSource;
import io.humb1t.file.factory.FileFactory;
import io.humb1t.shop.factory.FoodFactory;
import io.humb1t.shop.model.ShopPosition;
import io.humb1t.shop.model.food.Donut;
import io.humb1t.shop.model.manufacturer.Manufacturer;
import io.humb1t.shop.model.manufacturer.ManufacturerLocation;

import java.io.*;
import java.util.Objects;

public class Task4 {
    private static final String FILE_NAME = "commodities.data";

    public static void main(String[] args) {
        FileFactory fileFactory = new FileFactory();
        File file = fileFactory.getFile(FileSource.FILE_SYSTEM, FILE_NAME);
        FoodFactory foodFactory = new FoodFactory();
        Manufacturer manufacturer = new Manufacturer("ThiefGuild", new ManufacturerLocation("Riften"));
        Donut donut = foodFactory.createDonut(manufacturer, "Sweetroll");
        ShopPosition firstPosition = new ShopPosition(donut, 10.1, 5);
        ShopPosition secondPosition = new ShopPosition(donut, 20.5, 10);
        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(file))) {
            output.writeObject(firstPosition);
            output.writeObject(secondPosition);
            output.flush();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        ShopPosition serializedFirstPosition = null;
        ShopPosition serializedSecondPosition = null;
        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(file))) {
            serializedFirstPosition = (ShopPosition) input.readObject();
            serializedSecondPosition = (ShopPosition) input.readObject();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        checkPositions(firstPosition, serializedFirstPosition);
        checkPositions(secondPosition, serializedSecondPosition);
    }

    private static void checkPositions(ShopPosition position, ShopPosition serializedPosition) {
        String params = String.format("position:%s, serializedPosition:%s", position, serializedPosition);
        if (position == null || serializedPosition == null) {
            throw new IllegalArgumentException(params);
        }
        System.out.println(position);
        System.out.println(serializedPosition);
        System.out.println("Equality by 'equals': " + Objects.equals(position.getAbstractProduct(), serializedPosition.getAbstractProduct()));
        System.out.println("Equality by '==': " + (position.getAbstractProduct() == serializedPosition.getAbstractProduct()));
    }
}
