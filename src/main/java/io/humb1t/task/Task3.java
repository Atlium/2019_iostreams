package io.humb1t.task;

import io.humb1t.file.FileSource;
import io.humb1t.file.factory.FileFactory;
import io.humb1t.shop.factory.FoodFactory;
import io.humb1t.shop.model.ShopPosition;
import io.humb1t.shop.model.manufacturer.Manufacturer;
import io.humb1t.shop.model.manufacturer.ManufacturerLocation;
import io.humb1t.utils.DoubleUtils;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Task3 {

    private static final String FILE_NAME = "commodities.data";

    public static void main(String[] args) {
        FileFactory fileFactory = new FileFactory();
        File file = fileFactory.getFile(FileSource.FILE_SYSTEM, FILE_NAME);
        FoodFactory foodFactory = new FoodFactory();
        Manufacturer manufacturer = new Manufacturer("ThiefGuild", new ManufacturerLocation("Riften"));
        List<ShopPosition> shopPositionList = IntStream.range(0, 5)
                .mapToObj(i -> new ShopPosition(foodFactory.createDonut(manufacturer, "Sweetroll"), DoubleUtils.randomWithRange(1, 10), i + 1))
                .collect(Collectors.toList());
        try (DataOutputStream output = new DataOutputStream(new FileOutputStream(file))) {
            for (ShopPosition shopPosition : shopPositionList) {
                output.writeUTF(shopPosition.getAbstractProduct().getName());
                output.writeDouble(shopPosition.getPrice());
                output.writeInt(shopPosition.getCount());
            }
            output.flush();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try (DataInputStream input = new DataInputStream(new FileInputStream(file))) {
            while (input.available() > 0) {
                System.out.println("Name: " + input.readUTF());
                System.out.println("Price: " + input.readDouble());
                System.out.println("Count: " + input.readInt());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
