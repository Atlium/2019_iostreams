package io.humb1t.utils;

import java.util.Random;

public class DoubleUtils {
    private static final Random random = new Random();

    private DoubleUtils() {
    }

    public static double randomWithRange(double min, double max) {
        double randomValue = min + (max - min) * random.nextDouble();
        return (int) (Math.round(Math.abs(randomValue) * 100)) / 100.0;
    }
}
