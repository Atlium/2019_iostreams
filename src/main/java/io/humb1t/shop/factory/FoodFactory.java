package io.humb1t.shop.factory;

import io.humb1t.shop.model.manufacturer.Manufacturer;
import io.humb1t.shop.model.food.Donut;

public class FoodFactory {
    public Donut createDonut(Manufacturer manufacturer, String name) {
        return new Donut(manufacturer, name);
    }
}
