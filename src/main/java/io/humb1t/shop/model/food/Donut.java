package io.humb1t.shop.model.food;

import io.humb1t.shop.model.manufacturer.Manufacturer;
import io.humb1t.shop.model.product.AbstractProduct;
import io.humb1t.shop.model.product.ProductType;

import java.util.Objects;

public class Donut extends AbstractProduct {
    private static final long serialVersionUID = -7714173070629953747L;
    private final String name;

    public Donut(Manufacturer manufacturer, String name) {
        super(ProductType.FOOD, manufacturer);
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Donut)) return false;
        Donut donut = (Donut) o;
        return getProductType() == donut.getProductType() &&
                Objects.equals(getManufacturer(), donut.getManufacturer());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getProductType(), getManufacturer());
    }


    @Override
    public String toString() {
        return "Donut{" +
                "productType=" + getProductType() +
                ", manufacturer=" + getManufacturer() +
                "} " + super.toString();
    }
}
