package io.humb1t.shop.model;

import io.humb1t.shop.model.product.AbstractProduct;

import java.io.Serializable;
import java.util.Objects;

public class ShopPosition implements Serializable {
    private static final long serialVersionUID = -3461042410539337046L;
    private final AbstractProduct abstractProduct;
    private double price;
    private int count;

    public ShopPosition(AbstractProduct abstractProduct, double price, int count) {
        this.abstractProduct = abstractProduct;
        this.price = price;
        this.count = count;
    }

    public AbstractProduct getAbstractProduct() {
        return abstractProduct;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShopPosition)) return false;
        ShopPosition shopPosition = (ShopPosition) o;
        return Double.compare(shopPosition.getPrice(), getPrice()) == 0 &&
                getCount() == shopPosition.getCount() &&
                Objects.equals(getAbstractProduct(), shopPosition.getAbstractProduct());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getAbstractProduct(), getPrice(), getCount());
    }

    @Override
    public String toString() {
        return "ShopPosition{" +
                "abstractProduct=" + abstractProduct +
                ", price=" + price +
                ", count=" + count +
                '}';
    }
}
