package io.humb1t.shop.model.product;

public enum ProductType {
    FOOD, HOUSE_HOLD
}
