package io.humb1t.shop.model.product;

import io.humb1t.shop.model.manufacturer.Manufacturer;

public interface Product {
    String getName();

    ProductType getProductType();

    Manufacturer getManufacturer();
}
