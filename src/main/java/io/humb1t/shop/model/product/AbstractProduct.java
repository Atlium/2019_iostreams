package io.humb1t.shop.model.product;

import io.humb1t.shop.model.manufacturer.Manufacturer;

import java.io.Serializable;
import java.util.Objects;

public abstract class AbstractProduct implements Product, Serializable {
    private static final long serialVersionUID = 6458854774022878070L;
    private final ProductType productType;
    private final Manufacturer manufacturer;

    public AbstractProduct(ProductType productType, Manufacturer manufacturer) {
        this.productType = productType;
        this.manufacturer = manufacturer;
    }

    @Override
    public ProductType getProductType() {
        return productType;
    }

    @Override
    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractProduct)) return false;
        AbstractProduct that = (AbstractProduct) o;
        return getProductType() == that.getProductType() &&
                Objects.equals(getManufacturer(), that.getManufacturer());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getProductType(), getManufacturer());
    }

    @Override
    public String toString() {
        return "AbstractProduct{" +
                "productType=" + productType +
                ", manufacturer=" + manufacturer +
                '}';
    }
}
