package io.humb1t.shop.model.manufacturer;

import java.io.Serializable;
import java.util.Objects;

public class ManufacturerLocation implements Serializable {
    private static final long serialVersionUID = -9161799362990316819L;
    private final String name;

    public ManufacturerLocation(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ManufacturerLocation)) return false;
        ManufacturerLocation that = (ManufacturerLocation) o;
        return Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName());
    }

    @Override
    public String toString() {
        return "ManufacturerLocation{" +
                "name='" + name + '\'' +
                '}';
    }
}
