package io.humb1t.shop.model.manufacturer;

import java.io.Serializable;
import java.util.Objects;

public class Manufacturer implements Serializable {
    private static final long serialVersionUID = -3207016478726002117L;
    private final String name;
    private final ManufacturerLocation manufacturerLocation;

    public Manufacturer(String name, ManufacturerLocation manufacturerLocation) {
        this.name = name;
        this.manufacturerLocation = manufacturerLocation;
    }

    public String getName() {
        return name;
    }

    public ManufacturerLocation getManufacturerLocation() {
        return manufacturerLocation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Manufacturer)) return false;
        Manufacturer that = (Manufacturer) o;
        return Objects.equals(getName(), that.getName()) &&
                Objects.equals(getManufacturerLocation(), that.getManufacturerLocation());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName(), getManufacturerLocation());
    }

    @Override
    public String toString() {
        return "Manufacturer{" +
                "name='" + name + '\'' +
                ", manufacturerLocation=" + manufacturerLocation +
                '}';
    }
}
